Installation des cert-manager:
  - `kubectl create namespace cert-manager`
  - `helm repo add jetstack https://charts.jetstack.io`
  - `helm repo update`
  - `helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.1.0 --set installCRDs=true`

Dann: 
 - Issuer aus File anlegen
 - Certificate aus File anlegen (Evtl. muss auch erst der Cluster angelegt, dann die IP im Cert File angepasst und der Cluster nochmal reinstalled werden)


Installation der Bitnami Postgres:

`kubectl create namespace bitnami-postgres`

`helm install my-release bitnami/postgresql -n bitnami-postgres --set tls.enabled=true --set tls.certificatesSecret=bitnami-postgres-tls --set tls.certFilename=tls.crt --set tls.certKeyFilename=tls.key --set volumePermissions.enabled=true`

Port forwarden: `kubectl -n bitnami-postgres port-forward svc/my-release-postgresql 5432:5432`

PGAdmin downloaden und starten

Einloggen in die postgres-Datenbank durch
 - User: postgres
 - Password: Base64 dekodieren aus: `kubectl get secret my-release-postgresql -o jsonpath='{.data.postgresql-password}'`
